/**
 * General Purpose Utilities
 * String utilities
 * Header file
 *
 * @author Michal Bureš at FIT
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#ifndef STRING_UTILITIES_H

    #define STRING_UTILITIES_H

    struct substringCollection {
        int substringCount;
        char** substrings;
    };

    /**
     * Function splits a given string to a collection of substrings based on a collection of delimiter characters supplied by the delimiter string
     * 
     * @param inputString String to be split into substring collection
     * @param delimiterString String of delimiter characters. Each character of this string is a delimiter itself.
     * @return Data structure containing the count of the substrings and the substrings themselves
     */  
    struct substringCollection *splitStringIntoSubstringsBasedOnDelimiterString(char *inputString, char *delimiterString);

    /**
     * Function de-allocates memory of substring collection data structure.
     */ 
    void destructSubstringCollection(struct substringCollection *substringCollectionPointer);

#endif