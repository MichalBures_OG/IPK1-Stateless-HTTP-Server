# IPK – Project 1. – HTTP Server implemented in C
This is a HTTP server implemented in the C language – a project of **Michal Bureš**. The project has been done as part of studies at BUT FIT.

## Prerequisites
To run this server one needs:

* Shell
* Make
* C compiler

Linux Ubuntu OS is recommended for optimal experience.

## Setup
You have to build the server executable file using Make.

``` Shell
$ make clean-build
```

The `clean-build` target cleans the created `.o` files. The standard `make` does not.

Once You have built the executable file – run the server using:

``` Shell
$ /hinfosvc [port]
```

The `[port]` argument needs to be an integer. It is the port the server will start listening on.

## Function
The server provides information about the hardware of the computer it is running on.

### Base request URL
`http://hostname:port`

### Supported requests

`GET /hostname`

Returns a hostname of the computer – e.g. **merlin.fit.vutbr.cz**

`GET /cpu-name`

Returns a CPU model name of the computer – e.g. **Intel(R) Xeon(R) Silver 4214R CPU @ 2.40GHz**

`GET /load`

Returns instantaneous CPU load e.g. **8.69 %**

## Error Handling
There are several error categories:

* Unsupported request method – returns HTTP status code **405**
* Unsupported endpoint – returns HTTP status code **404**
* Other errors – outputs an error message and terminates execution

## Notes regarding implementation

### Methods of gathering information about the computer the server runs on
The used method is quite simple and platform dependent. The serves parses content of special Unix files:

* Hostname – **/etc/hostname**
* CPU information –  **/proc/cpuinfo**

The contents of these files are processed in a Unix pipeline. The server uses a special function based on the POSIX `FILE *popen(const char *command, const char *type)` function to do that.

The method of getting the CPU load information is a bit more sophisticated. The serves invokes the `ps` command and takes the CPU load of the 20 most CPU-intensive processes currently running on the computer. The server then performs the final load calculation considering the number of CPU threads.

### Desired future improvements
The is a thing the server is missing – **signal handling**. I failed making a signal handler for the `SIG_INT` signal.

---