/**
 * Stateless HTTP Server Implementation
 * Core Module
 * Header file
 *
 * @author Michal Bureš at FIT
 */

#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#ifndef SERVER_IMPLEMENTATION_H

    #define SERVER_IMPLEMENTATION_H

    #define procedure void

    #define REQUEST_QUEUE_LIMIT 10

    #define REQUEST_BUFFER_SIZE 1024
    #define REQUEST_RESPONSE_BUFFER_SIZE 256
    #define CONTENT_LENGTH_BUFFER_SIZE 10
    #define HOSTNAME_BUFFER_SIZE 256
    #define CPU_INFORMATION_BUFFER_SIZE 128
    #define CPU_MODEL_BUFFER_SIZE 48
    #define CPU_FREQUENCY_BUFFER_SIZE 32
    #define STANDARD_BUFFER_SIZE_32B 32

    struct serverData {
        int serverSocketFileDescriptor;
        struct sockaddr_in address;
    };

    struct requestProcessingResult {
        int httpCode;
        char *responseBody;
    };

    /**
     * Sets a given socket option to a given state (ON | OFF)
     * 
     * @param socketFileDescriptor The file descriptor of a socket
     * @param optionKey The option key -- an integer supplied by the option macro
     * @param optionState The new state of the option -- 1: ON 0: OFF
     */
    #define setSocketOption(socketFileDescriptor, optionKey, optionState)                          \
        if (setsockopt(socketFileDescriptor, SOL_SOCKET, optionKey, &(int){1}, sizeof(int)) < 0) { \
            close(socketFileDescriptor);                                                           \
            terminateDueToError("Socket option could not be set.", SOCKET_OPTION_ERROR);           \
        }

#endif
