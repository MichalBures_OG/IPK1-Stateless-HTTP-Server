/**
 * Stateless HTTP Server Implementation
 * Error Handling and Prevention Module
 * Header file
 *
 * @author Michal Bureš at FIT
 */

#ifndef ERROR_HANDLING_H

    #define ERROR_HANDLING_H

    #include <stdio.h>
    #include <stdlib.h>
    #include <stdnoreturn.h>

    enum ReturnCode {

        OK = 0,
        UNSPECIFIED_ERROR = 1,
        MALLOC_ERROR = 2,
        SOCKET_CREATION_ERROR = 3,
        SOCKET_OPTION_ERROR = 4,
        SOCKET_BINDING_ERROR = 5,
        UNACCEPTED_REQUEST_ERROR = 6,
        REQUEST_INFORMATION_READING_ERROR = 7,
        INVALID_REQUEST_LINE_ERROR = 8,
    };

    /**
     * Handles malloc error.
     * Calls terminateDueToError() internally.
     */
    noreturn void terminateDueToMallocError(const char *location);

    /**
     * Handles an error.
     * Calls perror internally.
     */
    noreturn void terminateDueToError(const char *message, const int returnCode);

    /**
     * Safely allocates a field of certain data type. The field is a pointer of specified data type.
     * 
     * @param field The name of the field
     * @param dataType The data type of the field
     * @param sizeOfOperand The operand for the sizeof operator. Specifies the size of memory allocation.
     */
    #define allocate(field, dataType, sizeOfOperand)     \
        dataType *field = malloc(sizeof(sizeOfOperand)); \
        if (field == NULL) {                             \
            terminateDueToMallocError(__func__);         \
        }

    /**
     * Safely allocates a field of certain data type. The field is a pointer of specified data type.
     * 
     * @param field The name of the field
     * @param dataType The data type of the field
     * @param byteCount Number of bytes to allocate. Specifies the size of memory allocation.
     */
    #define allocateBytes(field, dataType, byteCount)    \
        dataType *field = malloc(byteCount);             \
        if (field == NULL) {                             \
            terminateDueToMallocError(__func__);         \
        }

    /**
     * Safely allocates a field of certain data type. There is no additional reference. This is useful for pre-defined pointer types.
     * 
     * @param field The name of the field
     * @param dataType The data type of the field
     * @param sizeOfOperand The operand for the sizeof operator. Specifies the size of memory allocation.
     */
    #define allocateNoReference(field, dataType, sizeOfOperand) \
        dataType field = malloc(sizeof(sizeOfOperand));         \
        if (field == NULL) {                                    \
            terminateDueToMallocError(__func__);                \
        }

#endif
