/**
 * Stateless HTTP Server Implementation
 * Core Module
 * Implementation file
 *
 * @author Michal Bureš at FIT
 */

#include "errorHandling.h"
#include "serverImplementation.h"
#include "stringUtilities.h"

struct serverData setupHTTPServer(const int port);
procedure handleIncomingRequest(struct serverData *server);
struct requestProcessingResult *processIncomingRequest(char *httpRequestInformation);
char *getCPUInformation();
char *getCPULoadInformation();
char *getHostnameInformation();
char *getCommandOutput(const char *command, size_t bytes);

/**
 * Program entrypoint and main execution loop
 */ 
int main (int argc, char *argv[]) {

    int port = argc == 2 ? atoi(argv[1]) : 8080;
    port = port > 0 ? port : 8080;

    struct serverData server = setupHTTPServer(port);

    while (true) {
        handleIncomingRequest(&server);
    }

    return OK;
}

/**
 * Function that sets up a HTTP server using the <sys/socket.h> library
 * 
 * @param port Integer that represents a desired port of the server
 * @return Data structure containing all needed information about the server
 */ 
struct serverData setupHTTPServer(const int port) {

    printf("Hello World!\n");
    printf("This is hinfosvc - a Stateless HTTP Server Implementation!\n");

    printf("Initiating server setup ...\n");
    printf("The port is: %d\n", port);

    int serverSocketFileDescriptor;
    if ((serverSocketFileDescriptor = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
        terminateDueToError("Socket creation failed.", SOCKET_CREATION_ERROR);
    }

    printf("Socket created sucessfully.\n");
    printf("Socket file descriptor: %d\n", serverSocketFileDescriptor);

    printf("Trying to set a socket option: SO_REUSEADDR\n");
    setSocketOption(serverSocketFileDescriptor, SO_REUSEADDR, 1)
    printf("Socket option set sucessfully.\n");

    struct sockaddr_in address;
    int addrlen = sizeof(address);

    address.sin_family = PF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);

    if (bind(serverSocketFileDescriptor, (struct sockaddr *)&address, addrlen) < 0) {
        close(serverSocketFileDescriptor);
        terminateDueToError("Socket binding failed.", SOCKET_CREATION_ERROR);
    }

    printf("Socket binding sucessful.\n");

    if (listen(serverSocketFileDescriptor, REQUEST_QUEUE_LIMIT) < 0) {
        close(serverSocketFileDescriptor);
        terminateDueToError("Socket could not be set to listen.", SOCKET_CREATION_ERROR);
    }

    printf("Socket is listening. Request queue length sent to %d.\n", REQUEST_QUEUE_LIMIT);
    printf("Entering the main loop. Waiting for incoming requests.\n");

    struct serverData server;
    server.serverSocketFileDescriptor = serverSocketFileDescriptor;
    server.address = address;
    return server;
}

/**
 * Procedure triggered in a loop that handles the next incoming HTTP request
 * 
 * @param server Pointer to the HTTP server data structure
 */ 
procedure handleIncomingRequest(struct serverData *server) {

    int clientSocketFileDescriptor;
    if ((clientSocketFileDescriptor = accept(server->serverSocketFileDescriptor, NULL, 0)) < 0) {
        close(server->serverSocketFileDescriptor);
        terminateDueToError("Socket could not accept a request.", SOCKET_CREATION_ERROR);
    }

    allocateBytes(httpRequestInformation, char, REQUEST_BUFFER_SIZE)
    memset(httpRequestInformation, 0, REQUEST_BUFFER_SIZE);
    if (read(clientSocketFileDescriptor, httpRequestInformation, REQUEST_BUFFER_SIZE) < 0) {
        close(server->serverSocketFileDescriptor);
        close(clientSocketFileDescriptor);
        terminateDueToError("Could not read request information.", REQUEST_INFORMATION_READING_ERROR);
    }
    printf("HTTP request information:\n[%s]\n", httpRequestInformation);

    struct requestProcessingResult *requestResult = processIncomingRequest(httpRequestInformation);
    char *statusCodeString;
    switch (requestResult->httpCode) {
        case 200: 
            statusCodeString = "200 OK";
            break;
        case 404:
            statusCodeString = "404 Not Found";
            break;
        case 405:
            statusCodeString = "405 Method Not Allowed";
            break;
        default:
            statusCodeString = "400 Bad Request";
    }

    allocateBytes(requestResponse, char, REQUEST_RESPONSE_BUFFER_SIZE)
    strcpy(requestResponse, "HTTP/1.1 ");

    strcat(requestResponse, statusCodeString);
    strcat(requestResponse, "\nContent-Length: ");

    allocateBytes(contentLengthString, char, CONTENT_LENGTH_BUFFER_SIZE)
    sprintf(contentLengthString, "%d", (int)strlen(requestResult->responseBody));
    
    strcat(requestResponse, contentLengthString);
    strcat(requestResponse, "\nContent-Type: text/plain; charset=utf-8\n\n");
    strcat(requestResponse, requestResult->responseBody);
    
    printf("Sending response: [%s]\n", requestResponse);

    send(clientSocketFileDescriptor, requestResponse, strlen(requestResponse), 0);

    close(clientSocketFileDescriptor);

    free(contentLengthString);
    free(requestResponse);

    if (requestResult->httpCode == 200) {
        free(requestResult->responseBody);
    }

    free(requestResult);
    free(httpRequestInformation);
}

/**
 * Function that does the actual request processing.
 * 
 * @param httpRequestInformation String containing all information about the incoming HTTP request
 * @return Data structure that contains the HTTP code and body of the request response
 */ 
struct requestProcessingResult *processIncomingRequest(char* httpRequestInformation) {

    struct substringCollection *requestInformationLines = splitStringIntoSubstringsBasedOnDelimiterString(httpRequestInformation, "\r\n");
    char *requestLine = requestInformationLines->substrings[0];
    printf("The request line: [%s]\n", requestLine);

    struct substringCollection *coreInformationStringsOfRequestLine = splitStringIntoSubstringsBasedOnDelimiterString(requestLine, " ");
    if (coreInformationStringsOfRequestLine->substringCount != 3) {
        terminateDueToError("The request line of the incoming HTTP request is invalid.", INVALID_REQUEST_LINE_ERROR);
    }

    const char *method = coreInformationStringsOfRequestLine->substrings[0];
    const char *endpoint = coreInformationStringsOfRequestLine->substrings[1];

    printf("Request method: [%s]\n", method);
    printf("Target endpoint: [%s]\n", endpoint);

    allocate(result, struct requestProcessingResult, struct requestProcessingResult)
    result->responseBody = "";

    if (strcmp(method, "GET") != 0) {
        result->httpCode = 405;
    } else {
        if (strcmp(endpoint, "/hostname") == 0) {
            result->httpCode = 200;
            result->responseBody = getHostnameInformation();
        } else if (strcmp(endpoint, "/cpu-name") == 0) {
            result->httpCode = 200;
            result->responseBody = getCPUInformation();
        } else if (strcmp(endpoint, "/load") == 0) {
            result->httpCode = 200;
            result->responseBody = getCPULoadInformation();
        } else {
            result->httpCode = 404;
        }
    }

    destructSubstringCollection(requestInformationLines);
    destructSubstringCollection(coreInformationStringsOfRequestLine);

    return result;
}

/**
 * Function that returns a string of CPU information (CPU model name)
 */ 
char *getCPUInformation() {

    const char *getCPUInformationUsingGrep = "grep 'model name' /proc/cpuinfo | uniq | cut -c 14-";
    char *cpuInformationString;
    if ((cpuInformationString = getCommandOutput(getCPUInformationUsingGrep, CPU_INFORMATION_BUFFER_SIZE)) == NULL) {
        return "Could not retrieve CPU information.";
    }

    return cpuInformationString;
}

/**
 * Function that returns a string of CPU load information (2 decimal places) (percentage)
 */ 
char *getCPULoadInformation() {

    const char *getCPULoadOf20TopProcessesUsingPS = "ps -eo %cpu --sort=-%cpu | head -20 | sed '1d' | paste -sd+ - | bc";
    char *cummulativeCPULoadString;
    if ((cummulativeCPULoadString = getCommandOutput(getCPULoadOf20TopProcessesUsingPS, STANDARD_BUFFER_SIZE_32B)) == NULL) {
        return "Could not retrieve CPU Load information.";
    }

    const char *getCPUThreadCountUsingGrep = "grep -c processor /proc/cpuinfo";
    char *cpuThreadCountString;
    if ((cpuThreadCountString = getCommandOutput(getCPUThreadCountUsingGrep, STANDARD_BUFFER_SIZE_32B)) == NULL) {
        return "Could not retrieve CPU Load information.";
    }

    int cpuThreadCount = atoi(cpuThreadCountString);
    float cummulativeCPULoad = strtof((cummulativeCPULoadString), NULL);

    free(cummulativeCPULoadString);
    free(cpuThreadCountString);

    float cpuLoad = cummulativeCPULoad / cpuThreadCount;
    cpuLoad = cpuLoad > 100.0 ? 100.0 : cpuLoad;
    cpuLoad = cpuLoad < 0.0 ? 0.0 : cpuLoad;
    
    allocateBytes(cpuLoadString, char, STANDARD_BUFFER_SIZE_32B);
    snprintf(cpuLoadString, STANDARD_BUFFER_SIZE_32B, "%.2f %%\n", cpuLoad);

    return cpuLoadString;
}

/**
 * Function that returns a hostname as a string
 */ 
char *getHostnameInformation() {

    const char *readETCHostnameFile = "cat /etc/hostname";
    char *hostnameInformationString;
    if ((hostnameInformationString = getCommandOutput(readETCHostnameFile, CPU_INFORMATION_BUFFER_SIZE)) == NULL) {
        return "Could not retrieve hostname.";
    }

    return hostnameInformationString;
}

/**
 * POSIX popen() based function that executes a given command and returns the command output as a string
 * 
 * @param command Shell command string
 * @param bytes How many bytes of the output the function should save
 * @return The string contaning given amount of bytes of the command output
 */ 
char *getCommandOutput(const char* command, size_t bytes) {

    FILE *processPipe;

    if ((processPipe = (FILE*)popen(command, "r")) == 0) {
        return NULL;
    }

    allocateBytes(commandOutputString, char, bytes);
    memset(commandOutputString, 0, bytes);

    int nextCharacter;
    for (int byteIndex = 0; byteIndex < (int)bytes; byteIndex++) {
        nextCharacter = fgetc(processPipe);
        if (nextCharacter == EOF) {
            break;
        }
        commandOutputString[byteIndex] = nextCharacter;
    }
    commandOutputString[((int)bytes) - 1] = '\0';

    pclose(processPipe);

    return commandOutputString;
}