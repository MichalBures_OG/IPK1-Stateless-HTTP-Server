/**
 * Stateless HTTP Server Implementation
 * Error Handling and Prevention Module
 * Implementation file
 *
 * @author Michal Bureš at FIT
 */

#include "errorHandling.h"

noreturn void terminateDueToMallocError(const char *location) {
    terminateDueToError(location, MALLOC_ERROR);
}

noreturn void terminateDueToError(const char *message, const int returnCode) {
    perror(message);
    exit(returnCode);
}
