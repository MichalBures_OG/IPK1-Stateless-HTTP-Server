# Project Makefile
# Stateless HTTP Server Implementation
# Michal Bureš at FIT

LC_ALL=cs_CZ.utf8

CC = gcc
CFLAGS = -g -pedantic -Wall -Wextra

OBJS = serverImplementation.o errorHandling.o stringUtilities.o

all: $(OBJS)
	$(CC) $(CFLAGS) serverImplementation.o errorHandling.o stringUtilities.o -o hinfosvc

serverImplementation.o: serverImplementation.c serverImplementation.h
	$(CC) $(CFLAGS) -c serverImplementation.c
errorHandling.o: errorHandling.c errorHandling.h
	$(CC) $(CFLAGS) -c errorHandling.c
stringUtilities.o: stringUtilities.c stringUtilities.h
	$(CC) $(CFLAGS) -c stringUtilities.c
	
clean-build: all clean

clean:
	rm -f *.o
