/**
 * General Purpose Utilities
 * String utilities
 * Implementation file
 *
 * @author Michal Bureš at FIT
 */

#include "stringUtilities.h"

struct substringCollection *splitStringIntoSubstringsBasedOnDelimiterString(char *inputString, char *delimiterString) {

    int inputLength = (int)strlen(inputString);
    int delimiterLength = (int)strlen(delimiterString);

    struct substringCollection *substringCollectionPointer = malloc(sizeof(struct substringCollection));
    if (substringCollectionPointer == NULL) {
        fprintf(stderr, "Memory allocation has failed in function %s\n", __func__);
        exit(1);
    }

    substringCollectionPointer->substringCount = 1;

    bool *shouldPassTrough = malloc(inputLength * sizeof(bool));
    if (shouldPassTrough == NULL) {
        fprintf(stderr, "Memory allocation has failed in function %s\n", __func__);
        exit(1);
    }

    memset(shouldPassTrough, 1, inputLength);

    int maximumSubstringLength = 0;
    int currentSubstringLength = 0;

    bool lastCharacterIsDelimiterCharacter = false;

    for (int inputStringIndex = 0; inputStringIndex < inputLength; inputStringIndex++) {

        char nextInputStringCharacter = inputString[inputStringIndex];
        bool characterConformity = false;

        for (int delimiterStringIndex = 0; delimiterStringIndex < delimiterLength; delimiterStringIndex++) {
            if (nextInputStringCharacter == delimiterString[delimiterStringIndex]) {
                characterConformity = true;
            }
        }

        shouldPassTrough[inputStringIndex] = !characterConformity;
        lastCharacterIsDelimiterCharacter = characterConformity;
        substringCollectionPointer->substringCount += inputStringIndex > 0 && !shouldPassTrough[inputStringIndex] && shouldPassTrough[inputStringIndex - 1] ? 1 : 0;

        if (!characterConformity) {
            currentSubstringLength++;
        } else {
            maximumSubstringLength = currentSubstringLength > maximumSubstringLength ? currentSubstringLength : maximumSubstringLength;
            currentSubstringLength = 0;
        }
    }

    substringCollectionPointer->substringCount -= lastCharacterIsDelimiterCharacter ? 1 : 0;
    maximumSubstringLength = currentSubstringLength > maximumSubstringLength ? currentSubstringLength : maximumSubstringLength;

    substringCollectionPointer->substrings = malloc(substringCollectionPointer->substringCount * sizeof(char *));
    if (substringCollectionPointer->substrings == NULL) {
        fprintf(stderr, "Memory allocation has failed in function %s\n", __func__);
        exit(1);
    }
    for (int substringIndex = 0; substringIndex < substringCollectionPointer->substringCount; substringIndex++) {
        substringCollectionPointer->substrings[substringIndex] = malloc(maximumSubstringLength + 1);
        if (substringCollectionPointer->substrings[substringIndex] == NULL) {
            fprintf(stderr, "Memory allocation has failed in function %s\n", __func__);
            exit(1);
        }
        memset(substringCollectionPointer->substrings[substringIndex], 0, maximumSubstringLength + 1);
    }

    bool ingoreFurtherCharactersUnlessPassing = false;

    int finalSubstringIndex = 0;
    int currentSubstringCharacterIndex = 0;
    for (int inputStringIndex = 0; inputStringIndex < inputLength; inputStringIndex++) {
        bool shouldTheCharacterPassThrough = shouldPassTrough[inputStringIndex];
        if (shouldTheCharacterPassThrough) {
            substringCollectionPointer->substrings[finalSubstringIndex][currentSubstringCharacterIndex++] = inputString[inputStringIndex];
            ingoreFurtherCharactersUnlessPassing = false;
        } else if (!ingoreFurtherCharactersUnlessPassing) {
            substringCollectionPointer->substrings[finalSubstringIndex][currentSubstringCharacterIndex] = '\0';
            finalSubstringIndex++;
            currentSubstringCharacterIndex = 0;
            ingoreFurtherCharactersUnlessPassing = true;
        }
    }

    free(shouldPassTrough);

    return substringCollectionPointer;
}

void destructSubstringCollection(struct substringCollection *substringCollectionPointer) {
    for (int substringIndex = 0; substringIndex < substringCollectionPointer->substringCount; substringIndex++) {
        free(substringCollectionPointer->substrings[substringIndex]);
    }
    free(substringCollectionPointer->substrings);
    free(substringCollectionPointer);
}